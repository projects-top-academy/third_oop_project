﻿#include <iostream>
#include <string>

/*

Создайте программу, имитирующую многоквартирный дом.

Необходимо иметь классы Человек, Квартира, Дом.
Класс Квартира содержит динамический массив объектов класса Человек.Класс Дом содержит массив объектов класса Квартира.
Каждый из классов содержит переменные - члены и функции - члены, которые необходимы для предметной области класса.Обращаем ваше внимание, что память под строковые значения выделяется динамически.
Не забывайте обеспечить классы различными конструкторами(конструктор копирования обязателен), деструкторами.
Класс Человек :

Поля:
идентификационный номер;
фамилия;
имя;
отчество(для фамилии, имени и отчества память выделяется динамически!);
дата рождения(рекомендуется создать дополнительный класс Дата(день, месяц, год));

Функции - члены:
конструктор с параметрами идентификационный номер, фамилия, имя, отчество, дата рождения.В конструкторе использовать список инициализаторов полей класса;
конструктор по умолчанию.В конструкторе использовать делегирование конструктора;
конструктор копирования;
деструктор;
функцию - член для подсчёта созданных экземпляров класса "Человек";
сеттеры / геттеры для соответствующих полей класса;
вывод на экран информации о человеке.

*/

class Date {
public:
	int day;
	int month;
	int year;

	Date(int d, int m, int y) : day(d), month(m), year(y) {}
};

class Person {
private:
	int id;
	std::string* surname;
	std::string* name;
	std::string* patronymic;
	Date birthDate;

	static int count;

public:
	Person(int id, const std::string& s, const std::string& n, const std::string& p, const Date& b) : id(id), surname(new std::string(s)), name(new std::string(n)), patronymic(new std::string(p)), birthDate(b) {
		count++;
	}

	Person() : Person(0, "", "", "", Date(0,0,0)) {}

	Person(const Person& other) : id(other.id), birthDate(other.birthDate) {
		surname = new std::string(*other.surname);
		name = new std::string(*other.name);
		patronymic = new std::string(*other.patronymic);
		count++;
	}

	~Person() {
		delete surname;
		delete name;
		delete patronymic;
		count--;
	}

	static int getCount() { return count; }

	void setId(int newId) { id = newId; }
	int getId() const { return id; }

	void setSurname(const std::string& s) { *surname = s; }
	const std::string& getSurname() const { return *surname; }

	void setName(const std::string& n) { *name = n; }
	const std::string& getName() const { return *name; }

	void setPatronymic(const std::string& p) { *patronymic = p; }
	const std::string& getPatronymic() const { return *patronymic; }

	void setBirthDate(const Date& b) { birthDate = b; }
	Date getBirthDate() const { return birthDate; }

	void printInfo() {
		std::cout << "ID: " << id << ", Full Name: " << *surname << " " << *name << " " << *patronymic << ", Birth Date: " << birthDate.day << "." << birthDate.month << "." << birthDate.year << "\n";
	}
};

int Person::count = 0;

int main() {
	Date date1(1, 1, 1990);
	Person person1(1, "Smith", "John", "James", date1);

	Person person2(person1);

	person1.printInfo();
	person2.printInfo();

	std::cout << "Total number of people: " << Person::getCount() << "\n";

	return 0;
}